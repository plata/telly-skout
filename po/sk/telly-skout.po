# translation of telly-skout.po to Slovak
# Roman Paholík <wizzardsk@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: telly-skout\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-11-18 00:43+0000\n"
"PO-Revision-Date: 2022-05-14 20:09+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.04.1\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Roman Paholík"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "wizzardsk@gmail.com"

#: main.cpp:56
#, kde-format
msgid "Convergent TV guide based on Kirigami"
msgstr ""

#: main.cpp:59
#, kde-format
msgid "Telly Skout"
msgstr "Telly Skout"

#: main.cpp:63
#, kde-format
msgid "© 2020 KDE Community"
msgstr "© 2020 KDE komunita"

#: qml/ChannelListDelegate.qml:53
#, fuzzy, kde-format
#| msgid "Favorite"
msgctxt "@info:tooltip"
msgid "Favorite"
msgstr "Obľúbené"

#: qml/ChannelListPage.qml:20
#, fuzzy, kde-format
#| msgid "Channels"
msgctxt "@title"
msgid "Channels"
msgstr "Kanály"

#: qml/ChannelListPage.qml:32
#, kde-format
msgid "Loading channels..."
msgstr ""

#: qml/ChannelTablePage.qml:19
#, fuzzy, kde-format
#| msgid "Favorites"
msgctxt "@title"
msgid "Favorites"
msgstr "Obľúbené"

#: qml/ChannelTablePage.qml:34
#, kde-format
msgid "Please select favorites"
msgstr ""

#: qml/ChannelTablePage.qml:132
#, fuzzy, kde-format
#| msgid "not available"
msgctxt "placeholder message"
msgid "Information not available"
msgstr "nedostupné"

#: qml/GroupListDelegate.qml:30
#, kde-format
msgid "Channels"
msgstr "Kanály"

#: qml/GroupListPage.qml:15
#, fuzzy, kde-format
#| msgid "Favorites"
msgctxt "@title"
msgid "Select Favorites"
msgstr "Obľúbené"

#: qml/GroupListPage.qml:24
#, kde-format
msgid "Loading groups..."
msgstr ""

#: qml/SettingsPage.qml:14
#, fuzzy, kde-format
#| msgid "Settings"
msgctxt "@title"
msgid "Settings"
msgstr "Nastavenia"

#: qml/SettingsPage.qml:17
#, kde-format
msgctxt "@title:group"
msgid "Appearance"
msgstr ""

#: qml/SettingsPage.qml:22
#, fuzzy, kde-format
#| msgid "Program"
msgctxt "@option:spinbox height of the program"
msgid "Program height:"
msgstr "Program"

#: qml/SettingsPage.qml:28
#, kde-format
msgctxt "Number in px/min"
msgid "%1px/min"
msgstr ""

#: qml/SettingsPage.qml:38
#, kde-format
msgctxt "@option:spinbox"
msgid "Column width:"
msgstr ""

#: qml/SettingsPage.qml:44 qml/SettingsPage.qml:60
#, kde-format
msgctxt "Number in px"
msgid "%1px"
msgstr ""

#: qml/SettingsPage.qml:54
#, kde-format
msgctxt "@option:spinbox"
msgid "Font size:"
msgstr ""

#: qml/SettingsPage.qml:69
#, fuzzy, kde-format
#| msgid "Program"
msgctxt "@title:group Programs as TV programs"
msgid "Programs"
msgstr "Program"

#: qml/SettingsPage.qml:74
#, kde-format
msgctxt "@option:spinbox"
msgid "Delete old programs after:"
msgstr ""

#: qml/SettingsPage.qml:78 qml/SettingsPage.qml:107
#, kde-format
msgctxt "Number in days"
msgid "%1 day"
msgid_plural "%1 days"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""

#: qml/SettingsPage.qml:90
#, kde-format
msgid "Fetcher:"
msgstr ""

#: qml/SettingsPage.qml:91
#, kde-format
msgctxt "@option:combobox"
msgid "The fetcher which loads the programs."
msgstr ""

#: qml/SettingsPage.qml:102
#, kde-format
msgctxt "@option:spinbox"
msgid "Prefetch:"
msgstr ""

#: qml/SettingsPage.qml:119
#, kde-format
msgctxt "@label:textbox"
msgid "File:"
msgstr ""

#: qml/SettingsPage.qml:143
#, kde-format
msgctxt "@action:button"
msgid "Open file dialog"
msgstr ""

#: qml/SettingsPage.qml:149
#, kde-format
msgid "XML files (*.xml)"
msgstr ""

#: qml/SettingsPage.qml:149
#, kde-format
msgid "All files (*)"
msgstr ""

#: qml/TellySkoutGlobalDrawer.qml:17
#, kde-format
msgid "Favorites"
msgstr "Obľúbené"

#: qml/TellySkoutGlobalDrawer.qml:27
#, kde-format
msgid "Select Favorites"
msgstr ""

#: qml/TellySkoutGlobalDrawer.qml:36
#, kde-format
msgid "Sort Favorites"
msgstr ""

#: qml/TellySkoutGlobalDrawer.qml:49 qml/TellySkoutGlobalDrawer.qml:52
#, kde-format
msgid "Settings"
msgstr "Nastavenia"

#: qml/TellySkoutGlobalDrawer.qml:55 qml/TellySkoutGlobalDrawer.qml:58
#, kde-format
msgid "About"
msgstr "O aplikácii"

#: tvspielfilmfetcher.cpp:30
#, kde-format
msgid "Germany"
msgstr "Nemecko"

#: tvspielfilmfetcher.cpp:293
#, kde-format
msgid "Original title: "
msgstr ""

#: tvspielfilmfetcher.cpp:305
#, kde-format
msgid "Country: "
msgstr ""

#: tvspielfilmfetcher.cpp:317
#, kde-format
msgid "Year: "
msgstr ""

#: tvspielfilmfetcher.cpp:329
#, kde-format
msgid "Duration: "
msgstr ""

#: tvspielfilmfetcher.cpp:341
#, kde-format
msgid "FSK: "
msgstr ""

#: xmltvfetcher.cpp:43
#, kde-format
msgid "XMLTV"
msgstr ""

#~ msgid "days"
#~ msgstr "dní"
